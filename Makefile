
SHORT_HASH=$(shell git log -1 --pretty=format:"%h")
GIT_TAG=$(shell git describe --tags)
TMPDIR=/tmp/journey_x

tarball:
	rm -rf ${TMPDIR}
	mkdir --parents ${TMPDIR}/usr/local/bin
	mkdir --parents ${TMPDIR}/var/www/journeyfeed
	mkdir --parents ${TMPDIR}/opt/journeyfeed
	cp -rv conf/journey_data/get_journey_data.php ${TMPDIR}/usr/local/bin
	cp -rv conf/wwwroot/* ${TMPDIR}/var/www/journeyfeed
	cp -rf conf/journey_data/get_journey_data_config.php.dist ${TMPDIR}/opt/journeyfeed
	cp -rf README.md ${TMPDIR}/opt/journeyfeed
	tar -C ${TMPDIR}/ -czvf ./nsb-journey-scripts_${GIT_TAG}-${SHORT_HASH}.tar.gz usr var opt
	tar -tvf ./nsb-journey-scripts_${GIT_TAG}-${SHORT_HASH}.tar.gz

clean:
	rm -rf *.tar.gz ${TMPDIR}

scrub: clean
