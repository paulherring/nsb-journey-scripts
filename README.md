# Deployment steps:

Drop `journey_data_3.*-<hash>.tar.gz` into /conf/boot/modules

## Configuration

This program will pull data from DNA, and if configured, eventually send out UDP packets for DCP by itself.


### DCP
- `opt/journeyfeed/get_journey_data_config.php.dist` contains an example file to be placed in `/conf/get_journey_data_config.php`. In order for the DCP packets to be sent, MUST must be set up correctly and put into `/conf`.

### Web
- The web API does not currently depend on this configuration, nor does it have any other internal configuration, beyond expecting `[ccu]designation` to be correctly configured.

# What's installed
- `/usr/local/bin/get_journey_data.php` is an executable which must be forked to background on startup of the CCU
  - `[files]usr/local/bin/get_journey_data.php` is the normal place for this sort of thing.
  - or maybe `@reboot /usr/bin/php /usr/local/bin/get_journey_data.php` if the module gets unzipped before `fcron` starts.

- `/var/www/journeyfeed` is the webroot of the API. The following URLs are available:
  - `http://host/journey_data.php`
  - `http://host/telemetry_data.php`
  - `http://host/train_id.php`


# Logs:

 - Location: `/var/log/get_journey.log`
 - Normally only `LOG_ERR` logged.
 - To enable debug/info output, delete/rename `/conf/.nd2syslog_disable_hack` (no restart of the script required)


# Other data:

- Other data may be found in `/var/local/php_journey/`


# Repository:

- https://gitlab.com/nd-dev/nsb-journey-scripts
