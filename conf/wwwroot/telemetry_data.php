<?PHP
/**
 * TITLE:		telemetry_data.php
 *
 * AUTHOR:		Michael White
 *
 * VERSION:		2.0
 *
 * DATE:		24/01/2017
 *
 * DESCRIPTION: telemetry_data.php is a script that gathers telemetry data available on the CCU. A JSON object is then created using this data and the object is exposed. The JSON object is made up of the following data: portalUsers, gpsData and wanData.
 *
 * Response:     A json object containing the following members:
 *               "portalUsers" - the current numer of users logged into the portal
 *               "gpsData" - the current gpsData
 *               "wanData" - the current wanData
 *               "error" - the String value "null" if all is well
 *
 * If less than 3 satelites are visible the GPS values cannot be trusted. An error state of "No satellite lock" will be returned.
 * If the speed is less than 2 the bearing value cannot be trusted. It will be set to -1.
 *
 */

$telem = [
	"error" => []
];

/****************************************************************************/
/******							 portalUsers						   ******/
/****************************************************************************/

/** @brief Count number of members in ipset
  * @return array
  */
function count_ipset(): array{
	$output=[];
	$worked=0;
	$ret = exec("ipset -L", $output, $worked);
	if ($worked != 0){
		echo "ipset failed\n";
		return ['error'=>['portalError'=> 'ipset execution failure']];
	}
	if ( ($Members = array_search("Members:", $output)) === false){
		return ['error'=>['portalError'=> 'ipset parse failure']];
	}
	return ['portalUsers'=> (count($output) - $Members -1)]; // number of lines, minus (zero-based-)line where "Members:" is, minus one
}

/****************************************************************************/
/******							 gpsData							   ******/
/****************************************************************************/

/** @brief pull gps data from get_journey_data
  * @return array
  */
function get_gps(): array{
	$gpsfile = '/var/local/php_journey/gps.json';
	if (!file_exists($gpsfile)){
		return ['error'=>['gpsError'=> "GPS file from get_journey_data missing: $gpsfile"]];
	}
	if (($data = file_get_contents($gpsfile)) === false){
		return ['error'=>['gpsError'=> "Failure to get contents of $gpsfile"]];
	}
	if (($ret = json_decode($data, true)??false) === false){
		return ['error'=>['gpsError'=> "JSON decode error: $gpsfile - $data"]];
	}
	if (isset($ret['error'])){
		return ['error'=>['gpsError'=> $ret['error']]];
	}
	if (!isset($ret['sat_use']))
		return ['error'=>['gpsError'=> 'No gps signal available']];

	// do some translation between get_journey_data, and what appeared to be needed.
	// in the original script. Get rid of these if not needed.
	if (isset($ret['lng']))
		$ret['lon'] = $ret['lng'];
	if (isset($ret['sat_use']))
		$ret['sats'] = $ret['sat_use'];
	else
		return ['error'=>['gpsError'=> 'No gps signal available']];

	if (isset($ret['kmh']))
		$ret['speed'] = $ret['kmh'];

	return ['gpsData' => $ret];
}

/****************************************************************************/
/******							 WAN load							   ******/
/****************************************************************************/

/** @brief go get some wan data
  * @retur array
  */
function wan_data(): array{
	if ( ($dirs = glob('/var/local/unified/*'))=== false || count($dirs)=== 0){
		return ['error'=>['wanError'=> "Nothing found in /var/local/unified "]];
	}

	$missing = false;
	$files = ["state", "ravg", "wavg", "ss"];
	$ret=[];
	foreach($dirs as $dir){
		$num = basename($dir);
		if ($num == "-1")
			continue; // ignore that one
		foreach($files as $file){
			$filename = $dir.'/'.$file;
			if (file_exists($filename) && ($data = file_get_contents($filename))!== false){
				$ret['wanData']['wan-'.$num][$file] = $data;
				continue;
			}
			$ret['wanData']['wan-'.$num][$file]  = "<missing>";
			$ret['error']['wanError'] = "Missing wanData";
		}
	}
	return $ret;
}

$telem = array_merge_recursive($telem, count_ipset()); // portal
$telem = array_merge_recursive($telem, get_gps()); // gps
$telem = array_merge_recursive($telem, wan_data()); // wan


/****************************************************************************/
header('Content-Type: application/json');
// echo json_encode($telem, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
echo json_encode($telem);


