<?PHP
/**
 * A script to display the train ID stored on local file.
 *
 * Display error if file cannot be found.
 *
 * Displays any errors that occur with journey information.
 *
 * v2.0
 */
header('Content-Type: application/json;charset=UTF-8');

$file = "/var/local/php_journey/train_id.json";

header('Content-Type: application/json;charset=UTF-8');
if(file_exists ($file)){
	if (($data = file_get_contents($file)) !== false){
		echo $data."\n";
	}else{
		echo json_encode(['error'=>"Unable to read $file"]);
	}
}else{
	echo json_encode(['error'=>"$file not found. Please run get_journey_data.php"]);
}
