#!/usr/bin/env php
<?php declare(strict_types = 1);

set_error_handler(
	function ($severity, $message, $file, $line) {
		throw new ErrorException($message, $severity, $severity, $file, $line);
	}
);

/** @brief loose syslog() facsimile. Logs DEBUG/INFO only if /conf/.nd2syslog_disable_hack
  * missing, and logs everything else
  * @param int $priority one of the syslog LOG_* defines
  * @param string $s what to send to log
  * @param mixed $retval what to return, to allow the construct `return log_message(LOG_DEBUG, "whatever", true);`. Defaults to false
  * @return mixed
  */
ini_set('date.timezone', 'UTC');
ini_set("log_errors", "1");
ini_set("error_log", "/var/log/get_journey_data.log");
function log_message(int $priority, string $s, $retval=false) {
	static $output=-1;
	if ($output === -1){ // decide whether to actually output anything
		$output = !file_exists('/conf/.nd2syslog_disable_hack');
	}

	if (!$output && $priority >= LOG_INFO)
		return $retval; // not bothered

	switch ($priority){
		case LOG_EMERG:		$pri="EMERG "; break;
		case LOG_ALERT:		$pri="ALERT "; break;
		case LOG_CRIT:		$pri="CRIT  "; break;
		case LOG_ERR:		$pri="ERROR "; break;
		case LOG_WARNING:	$pri="WARN  "; break;
		case LOG_NOTICE:	$pri="NOTICE"; break;
		case LOG_INFO:		$pri="INFO  "; break;
		case LOG_DEBUG:		$pri="DEBUG "; break;
		default:			$pri="UNK   "; break;
	}

	$micro_date = microtime();
	$date_array = explode(" ",$micro_date);
	$usec = $date_array[0];

	$bt = debug_backtrace();
	$caller = array_shift($bt); // want penultimate for file and line number
	$file = $caller['file'];
	$line = $caller['line'];
	$caller = array_shift($bt); // and the next for the calling function
	$func = $caller['function']??"";

	error_log($usec." [$pri]".basename($file).'::'.$func.'('.$line."): $s");
	return $retval;
}

/** @brief a  more vertically compact `print_r()` near-equivilant
 * @param mixed $var the thing to dump
 * @param bool $return false(default) output to stdout, true - return as string
 * @return string|bool
 */
function print_c($var, bool $return=false){
	$out=internal_compact_dump($var);
	if ($return)
		return $out;
	echo $out;
	return true;
}

/** @brief internal helper for above function.
  *	Not for external use. Unsuitable for infants under 12 months. Store in a cool place.
  *
  * @param mixed $var
  * @param string $key
  * @param int $indent
  * @return string
 */
function internal_compact_dump($var, $key='', $indent=0){
	if ($indent>10)
		return "too deep\n";
	$output = "";
	$tab = str_repeat('.',$indent*4);
	switch ( ($t=gettype($var)) ){
		case "integer":
		case "double":
		case "resource":
		case "resource (closed)":
			$output .= $tab.$key.": (".$t.") $var\n";
			break;
		case "NULL":
			$output .= $tab.$key.": NULL\n";
			break;
		case "boolean":
			if ($var===false)
				$var="false";
			$output .= $tab.$key.': (boolean) '.$var."\n";
			break;
		case "string":
			$output .= $tab.$key.': (string) "'.$var.'"'."\n";
			break;
		case "object":
			$output .= $tab.'[\''.$key.'\']'."#".get_class($var)."{\n";
			foreach($var as $key=>$val){
				$output .= internal_compact_dump($val, $key, $indent+1);
			}
			$output .= $tab.print_r($var, true)."\n"; // cheating.
			$output .= $tab."}\n";
			break;
		case "array":
			$output .= $tab.'[\''.$key.'\']'."{\n";
			foreach($var as $key=>$val){
				$output .= internal_compact_dump($val, $key, $indent+1);
			}
			$output .= $tab."}\n";
			break;
		default:
			$output .= $tab.$key.": (".$t.") <not attempting>\n";
			break;
	}
	return $output;
}

/** @brief Take a time in a variety of possbile formats and output in UTC
  * @param mixed $datetime the thing to convert
  * @return string|false
  */
function utc($datetime){
	$FMT = 'Y-m-d\TH:i:s\Z';
	switch (gettype($datetime)){
		case "string":
			return (new \DateTimeImmutable($datetime))->
									setTimezone(new \DateTimeZone('UTC'))->
									format($FMT);
		case "object":
			switch(get_class($datetime)){
				case "DateTime":
					return $datetime->
									setTimezone(new \DateTimeZone('UTC'))->
									format($FMT);
				default:
					log_message(LOG_ERR, "Couldn't convert type. No case for class:".get_class($datetime));
					return false;
			}
		case "boolean": // you shouldn't be passing this in
			log_message(LOG_INFO, "Bool passed in - we don't convert bools to time");
			return $datetime;
		case "integer": // presume epoch
			return (new \DateTimeImmutable())->
									setTimestamp($datetime)->
									format($FMT);
		default:
			log_message(LOG_ERR, "Couldn't convert type. No case for type:".gettype($datetime));
			return false;
	}
}

/** @brief Take a time and convert to epoch seconds
  * @param mixed $datetime the thing to convert
  * @return int|false
  */
function epoch($datetime){
	switch (gettype($datetime)){
		case "string":
			return (new \DateTimeImmutable($datetime))->getTimestamp();
		default:
			log_message(LOG_ERR, "Couldn't convert type. No case for type:".gettype($datetime));
			return false;
	}
}

/** @brief clamp a value
  * @param int $value the value to be clamped
  * @param int $min the minimum value to return
  * @param int $max the maximum value to return
  * @return int
  */
function clamp(int $value, int $min, int $max): int {
	return max($min, min($max, $value));
}

/** General GPS class.
 * - Gets current location from GGA string if usable, else RMC if that's usable. Else returns 0.
 * - Contains a 'distance' calculation using the Haversine formula
 */
class Gps {
	/** @var int */ private static $distance_precision = -1; // nearest 10m.
	/** @var int */ private static $gps_precision = 4; // about 11m. https://gis.stackexchange.com/a/8674

	/** @brief convert a NMEA-formatted lat/lng into a floating point
	  * @param string $nmea -  the number from the NMEA string in {D}DDmm.mmmm format
	  * @param string $direction - N/S/E/W
	  * @return float|null floating point number or null
	*/
	private static function nmea2latlng(string $nmea, string $direction): ?float {
		$sanitised = filter_var($nmea, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		if (strlen($sanitised) === 0){
			return null;
		}
		$degrees = intval(floatval($sanitised)/100); // pull degrees out
		$minutes = floatval($nmea) - $degrees*100; // now minutes
		$degrees += $minutes/60; // add minutes as fractions
		if ((strtoupper($direction) == "S") || (strtoupper($direction) == "W")){
			$degrees *= -1; // reverse direction
		}
		return round($degrees, self::$gps_precision);
	}

	/** @brief get lat/lng and sat in use from GGA
	  * @param string $root - where to look for the GGA file. Example alternative: /var/local/gps.default/
	  * @return array{lat: float, lng: float, sat_use: int}|null
	  */
	private static function gpgga(string $root='/var/local/'): ?array {
		static $debugout = 1;
		$lat = $lng = null;
		$qual = $sat = -1;
		if ( (($files = glob($root.'/??gga'))!==false) && (count($files)!==0)){
			foreach ($files as $file){
				$gga = explode(",", file_get_contents($file));
				if (count($gga) > 7){
					// http://aprs.gids.nl/nmea/#gga
					$lat = self::nmea2latlng($gga[2], $gga[3]);
					$lng = self::nmea2latlng($gga[4], $gga[5]);
					$qual =intval($gga[6]); // 0=invalid; 1=GPS fix; 2=Diff. GPS fix
					$sat = intval($gga[7]); // in use, (not in view.) <3 not much use
					if (($lat!==null) && ($lng!==null) && ($qual>0) && ($sat>=3)){
						$debugout = 1;
						return [
							'lat'=>floatval($lat),
							'lng'=>floatval($lng),
							'sat_use'=>intval($sat)];
					}
				}
				if ($debugout){
					$debug='';
					if (!$lat || !$lng)
						$debug.="No lat &/| lng present: lat:$lat, lng:$lng ";
					if ($qual <= 0)
						$debug.="Insuffient quality: $qual ";
					if ($sat <3)
						$debug.="Insuffient satellites: $sat ";
					log_message(LOG_DEBUG, $debug);
				}
			}
		}else{
			if ($debugout){
				log_message(LOG_DEBUG, "No match for $root/??gga");
			}
		}
		$debugout = 0;
		return null;
	}

	/** @brief get lat/lng and speed from RMC
	  * @param string $root - where to look for the GGA file. Example alternative: /var/local/gps.default/
	  * @return array{lat: float, lng: float}|null
	  */
	private static function gprmc(string $root='/var/local/'): ?array {
		static $debugout = 1;
		$lat = $lng = null;
		$valid = '';
		if ( (($files = glob($root.'/??rmc'))!==false) && (count($files)!==0)){
			foreach ($files as $file){
				$rmc = explode(",", file_get_contents($file));
				if (count($rmc) > 6){
					// http://aprs.gids.nl/nmea/#rmc
					$lat = self::nmea2latlng($rmc[3], $rmc[4]);
					$lng = self::nmea2latlng($rmc[5], $rmc[6]);
					$valid =$rmc[2]; // A valid, V invalid
					$knots = floatval($rmc[7]);
					if ($knots >=2){ // from original telemetry_data.php - if speed < 2, bearing not to be trusted so set bearing to -1
						$bearing = floatval($rmc[8]);
					}
					else{
						$bearing = -1;
					}
					if (($lat!==null) && ($lng!==null) && (strtoupper($valid) == 'A')){
						$debugout = 1;
						return [
							'lat'=>$lat,
							'lng'=>$lng,
							'kts'=>$knots,
							'kmh'=> round($knots * 1.852, 2), /* conversion factor */
							'mph'=> round($knots * 1.15078, 1), /* conversion factor */
							'bearing' =>$bearing ];
					}
				}
				if ($debugout){
					$debug='';
					if (!$lat || $lng)
						$debug.="No lat/lng present: lat:$lat, lng:$lng ";
					if (strtoupper($valid) != 'A')
						$debug.="Invalid: $valid ";
					log_message(LOG_DEBUG, $debug);
				}
			}
		}else{
			if ($debugout)
				log_message(LOG_DEBUG, "No match for $root/??rmc");
		}
		$debugout = 0;
		return null;
	}

	/** @brief get sat in view from GSV
	  * @param string $root - where to look for the GGA file. Example alternative: /var/local/gps.default/
	  * @return array{sat_view: int}|null
	  */
	private static function gpgsv(string $root='/var/local/'): ?array {
		static $debugout = 1;
		$valid = '';
		if ( (($files = glob($root.'/??gsv1'))!==false) && (count($files)!==0)){
			foreach ($files as $file){
				$gsv = explode(",", file_get_contents($file));
				if (count($gsv) > 3){
					// http://aprs.gids.nl/nmea/#gsv
					$sat = intval($gsv[3]);
					return ['sat_view'=>$sat];
				}
				if ($debugout){
					$debug='';
					$debug.="Short sentence";
					log_message(LOG_DEBUG, $debug);
				}
			}
		}else{
			if ($debugout)
				log_message(LOG_DEBUG, "No match for $root/??gsv1");
		}
		$debugout = 0;
		return null;
	}

	/** @brief get speed from VTG
	  * @param string $root - where to look for the GGA file. Example alternative: /var/local/gps.default/
	  * @return array{kts: float, kmh: float}|null
	  */
	private static function gpvtg(string $root='/var/local/'): ?array {
		static $debugout = 1;
		$lat = $lng = null;
		$valid = '';
		if ( (($files = glob($root.'/??vtg'))!==false) && (count($files)!==0)){
			foreach ($files as $file){
				$vtg = explode(",", file_get_contents($file));
				if (count($vtg) > 7){
					// http://aprs.gids.nl/nmea/#vtg
					$knots = $vtg[5];
					$kmh = $vtg[7];
					if ($knots!='' && $kmh != ''){
						return [
							'kts'=>floatval($knots),
							'kmh'=>floatval($kmh)];
					}
				}
				if ($debugout){
					$debug='';
					$debug.="Short sentence";
					log_message(LOG_DEBUG, $debug);
				}
			}
		}else{
			if ($debugout)
				log_message(LOG_DEBUG, "No match for $root/??vtg");
		}
		$debugout = 0;
		return null;
	}


	/** @brief get/set precision for self::distance()
	  * @param int $range if not set, then just return current. If positive, return to that many dp. If negative roudn to nearest 10^range.
	  * @return int current/last setting
	  */
	static function precision(int $range = PHP_INT_MAX): int {
		$tmp = self::$distance_precision;
		if ($range !== PHP_INT_MAX){
			if ($range < -5 /* 100Km */ || $range > 2 /* 1mm */){
				log_message(LOG_ERR, "Silly range - between -5 (100KM) or 2(1mm) please. $range supplied, not changing from ".self::$distance_precision);
			}else{
				self::$distance_precision = $range;
			}
		}
		return $tmp;
	}

	/** @brief get/set precision for self::distance()
	  * @param int $range if not set, then just return current. If positive, return to that many dp. If negative roudn to nearest 10^range.
	  * @return int current/last setting
	  */
	static function gps_precision(int $range = PHP_INT_MAX): int {
		$tmp = self::$gps_precision;
		if ($range !== PHP_INT_MAX){
			if ($range < 0 || $range > 6 /* 11 cm */){ // https://gis.stackexchange.com/a/8674 - again
				log_message(LOG_ERR, "Silly range - between 0(111KM) or 6(11cm) please. $range supplied, not changing from ".self::$gps_precision);
			}else{
				self::$gps_precision = $range;
			}
		}
		return $tmp;
	}

	/** @brief get GPS coords
	  * @param string $root - where to look for the NMEA files. Example alternative: /var/local/gps.default/
	  * @return array|null
	  */
	static function get_gps_data(string $root='/var/local'): ?array {
		// these should be in decreasing order of 'usefulness' - later ones will
		// not override earlier ones
		$gps_data = [];
		if (($data = self::gpgga($root)) !== null)
			$gps_data = array_merge($data, $gps_data);
		if (($data = self::gpgsv($root)) !== null)
			$gps_data = array_merge($data, $gps_data);
		if (($data = self::gpvtg($root)) !== null)
			$gps_data = array_merge($data, $gps_data);
		if (($data = self::gprmc($root)) !== null)
			$gps_data = array_merge($data, $gps_data);
		if (count($gps_data)===0)
			$gps_data = null;
		// log_message(LOG_DEBUG, "Returning ".print_c($gps_data, true));
		return $gps_data;
	}

	/** @brief pow(10, $x)
	  * @param int $x which power of 10 to return
	  * @return int|null 10^%x
	  */
	private static function pow10(int $x): ?int{
		switch ($x){
			case 0: return 1;
			case 1: return 10;
			case 2: return 100;
			case 3: return 1000;
			case 4: return 10000;
			case 5: return 100000;
		}
		return log_message(LOG_ERR, "Unsupported value - only [0-5] allowed - $x supplied", null);
	}

	/** @brief great-circle distance between two points on a sphere
	 * @param array{lat: float, lng: float}|null $point1 first point [lat,lng]; Should be specified, if null, returns null.
	 * @param array{lat: float, lng: float}|null $point2 second point [lat,lng] if supplied. Current location used if not. Must have both args if specified
	 * @param int $radius mean-radius of the earth (metres). In case it changes. Or this is used for Mars.
	 * @return float|null distance in metres, or null on bad input
	 */
	static function distance(array $point1=null, array $point2=null, int $radius=6371000):?float {
		if ((!$point1) || !isset($point1['lat']) || !isset($point1['lng']) ){
			log_message(LOG_DEBUG, "point1 invalid - null or missing lat&/|lng: ".print_c($point1, true));
			return null; // yeah, no. That's not gonna work.
		}
		if (($point2===null) && ($point2 = self::get_gps_data()) == null){
			return null; // we don't know where we are
		}else if (!isset($point2['lat']) || !isset($point2['lng'])){
			log_message(LOG_ERR, "point2 invalid - missing lat&/|lng: ".print_c($point2, true));
			return null; // that's not gonna work either
		}
		$lat1 = deg2rad($point1['lat']);
		$lng1 = deg2rad($point1['lng']);
		$lat2 = deg2rad($point2['lat']);
		$lng2 = deg2rad($point2['lng']);

		if (($lat1+$lng1)==0 || ($lat2+$lng2)==0){ // deliberately not ===
			// Station 13010 - Soul
			log_message(LOG_ERR, "Highly suspicious input: point1:".print_c($point1, true).", point2:".print_c($point2, true));
			return $radius; // return a massive distance. Alternative here would be to return 0, but ∞ works for this app.
		}
		// https://stackoverflow.com/a/14751773
		$dLat = $lat2 - $lat1;
		$dLng = $lng2 - $lng1;
		$angle = 2 * asin(sqrt(pow(sin($dLat / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($dLng / 2), 2)));
		return round( $angle * $radius, self::$distance_precision);
	}

	/** @brief debug-out a gps object
	  * @param array{lat: float, lng: float}|null $gps the gps to dump
	  * @param bool $return false(default) output to stdout, true - return as string
	  * @return string|bool
	  */
	public static function debug_gps(?array $gps, $return=false){
		if (!$gps){
			$msg = '[gps-null]';
		}else{
			$msg = "";
			if (isset($gps['lat']))
				$msg.=abs($gps['lat']).(($gps['lat']>0)?"N ":"S ");
			if (isset($gps['lng']))
				$msg.=abs($gps['lng']).(($gps['lng']>0)?"E ":"W ");
			if (isset($gps['kts']))
				$msg.=$gps['kts']."kts ";
			if (isset($gps['kmh']))
				$msg.=$gps['kmh']."km/h ";
			if (isset($gps['mph']))
				$msg.=$gps['mph']."mph ";
			if (isset($gps['sat_view']))
				$msg.=($gps['sat_use']??0).'/'.$gps['sat_view'].' sats';
		}
		if ($return)
			return $msg;
		return log_message(LOG_DEBUG, $msg);
	}
}

class Web{

	/** @var string */ static private $journeyUrl = 'https://nsb02.fleetmanager.nomadrail.com/platform/api/v2/journeys?graph=journeyLocations(location)+vehicleAssignments(consist)+origin+destination&page-size=100&q=date==P;vehicleAssignments.consist.id==';
	/** @var string */ static private $vehicleUrl = 'https://nsb02.fleetmanager.nomadrail.com/platform/api/v2/managedConsists?q=reference==';

	/** @var string */ static private $username = "Nomad.API";
	/** @var string */ static private $password = "TZR7HvrW";

	/** Set headers on a HTTP request
	  * @return resource
	  */
	private static function auth_ctx() {
		$opts = [
			'http' => [
				'method' => "GET",
				'header' => "Authorization: Basic " . base64_encode(self::$username.':'.self::$password)
			],
			'ssl'=> [
				// 'verify_peer' => false  ignore certificate errors
			]
		];
		return stream_context_create($opts);
	}


	/** @brief Retrieve JSON from a URI, or a cached version if it's recent enough
	  * @param string $uri the URI to get
	  * @param int $timeout how long to consider the cached URI to be fresh
	  * @return array<mixed>|null an associative array of the JSON on success, false otherwise
	  */
	private static function cachedjson(string $uri, int $timeout=3600 /* one hour */): ?array{
		$tmpfile = '/tmp/'.md5($uri);
		$raw = $tmpfile.'.raw';
		$pp = $tmpfile.'.pp';
		// exists, has a time, and recent?
		if ( (!file_exists($raw) || ($md5date = filemtime($raw)) === false) || (time() - $md5date) > $timeout){ // No. try and get it.
			try {
				$file = file_get_contents($uri, false, self::auth_ctx());
			}catch (Exception $e) {
				log_message(LOG_ERR, $e->getMessage());
				log_message(LOG_ERR, "URL: $uri");
				return null;
			}
			if (($ret = json_decode($file, true)??false) !== false){
				file_put_contents($raw, $file);
				file_put_contents($pp, json_encode($ret, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
			}
			return $ret;
		}else{
			return json_decode(file_get_contents($raw), true)??null; // Yes. Return cached version
		}
	}

	/** @brief Get consist data for a particular consit
	  * @param string $reference - typically the designation of the consist
	  * @return array<mixed>|null json array data on success
	  */
	public static function consist(string $reference): ?array{
		return self::cachedjson(self::$vehicleUrl.$reference);
	}

	/** @brief Get journey assignments for a particular consist
	  * @param string $consist_id - typically obtiained from self::consist()
	  * @return array<mixed>|null json array data on success
	  */
	public static function assignments(string $consist_id){
		return self::cachedjson(self::$journeyUrl.$consist_id);
	}
}

class Journeys{
	/** @brief "when" for journeys, and stations within journeys
	  */
	const FIRST = -9999;
	const PREVIOUS = -1;
	const CURRENT = 0;
	const NEXT = 1;
	const LAST = 9999;

	/** @var array<mixed> */ private $journeys;

	/* @brief A station's time is
	* 	- if stopping, the departure time if present, else the arrival time (or vice-versa)
	* 	- A passing time is just that.
	* Else return date far into the future.
	* @param mixed $station a single station
	* @param int $which which of departure or arrival to return if both present
	* @return
	*/
	const DEPARTURE = 1;
	const ARRIVAL = 2;
	static function station_epoch(array $station, int $which = self::DEPARTURE): int{
		$t='';
		switch ($type = $station['type']??null){
			case 'STOP':
				if ($which === self::DEPARTURE){
					if ( ($t = $station['plannedDepartureDateTime']??false) === false){
						if ( ($t = $station['plannedArrivalDateTime']??false) === false){
							log_message(LOG_ERR, "STOP with neither arrival or departure time: ".print_c($station['location'], true));
						}
					}
				}else{
					if ( ($t = $station['plannedArrivalDateTime']??false) === false){
						if ( ($t = $station['plannedDepartureDateTime']??false) === false){
							log_message(LOG_ERR, "STOP with neither arrival or departure time: ".print_c($station['location'], true));
						}

					}
				}
				break;
			case 'PASS':
				if ( ($t = $station['plannedPassDateTime']??false) === false){
					log_message(LOG_ERR, "PASS without a passing time: ".print_c($station['location'], true));
				}
				break;
			default:
				log_message(LOG_ERR, "Unrecogised station:".print_c($station, true));
				break;
		}
		if ($t === ''){
			log_message(LOG_DEBUG, "No time?");
			$t = "2037-12-31T23:59:59Z";// stick invalid states far into the future. Y2k38 problem though, on testing.
		}
		return epoch($t);
	}

	/** @brief usort function to sort journeys in ascending chronological order
	  * @param array<mixed> $left
	  * @param array<mixed> $right
	  * @return int
	  */
	static function journey_cmp(array $left, array $right): int{
		return epoch($left['origin']['plannedDepartureDateTime']??"2037-12-31T23:59:59Z") -
				epoch($right['origin']['plannedDepartureDateTime']??"2037-12-31T23:59:59Z");
	}
	/** @brief usort function to sort stations in ascending chronological order
	  * @param array<mixed> $left
	  * @param array<mixed> $right
	  * @return int
	  */
	static function station_cmp(array $left, array $right): int{
		return self::station_epoch($left) - self::station_epoch($right);
	}

	/** Sort the journeys into chronological order
	  */
	function sort(): bool {
		if (!$this->journeys){
			return log_message(LOG_INFO, "No journey data");
		}
		if (($this->journeys['totalCount']??0) === 0){
			return log_message(LOG_INFO, "No journeys in data");
		}

		// Journeys are, empirically observed, not guaranteed to be in chronological
		// order within $journeys['items']
		usort($this->journeys['items'], 'self::journey_cmp');

		foreach($this->journeys['items'] as $jidx => $journey){
			$this->journeys['items'][$jidx]['jidx'] = $jidx;
			// if ($jidx == 0)
			// 	log_message(LOG_DEBUG, "journey: ".print_c($journey, true));
			if (($journey['journeyLocations']['totalCount']??0) > 0){
				// Meanwhile, stations withinn journeys haven't been seen to be out of
				// order, but just in case...
				usort($journey['journeyLocations']['items'], 'self::station_cmp');
			}
		}

		return true;
	}


	/** @brief get a journey based on time
	  * @param int $which which journey. 0 = now
	  * @param int $when omcpared to to when? Defaults to now
	  * @return array|null journey, if one fits the request, null otherwise ("NEXT" when on last, "CURRENT" when in between journeys etc.)
	  */
	public function get_journey(int $which = self::CURRENT,int  $when = null): ?array{
		if (!$this->journeys || !($this->journeys['items']??null) || !($this->journeys['totalCount']??null))
			return log_message(LOG_DEBUG, "No journeys", null);

		if ($which == self::FIRST){
			return $this->journeys['items'][0];
		}
		if ($which == self::LAST){
			$last = $this->journeys['totalCount'] -1;
			return $this->journeys['items'][$last];
		}

		$leeway = 10*60; // 10 minutes.
		if (!$when)
			$when = time();
		$last = -1;
		$next = -1;
		$idx = -1;
		foreach($this->journeys['items'] as $jidx => $journey){
			$journeyStart = epoch($journey['origin']['plannedDepartureDateTime']) - $leeway;
			$journeyEnd = epoch($journey['destination']['plannedArrivalDateTime']) + $leeway;
			if (($when > $journeyStart) && ($when <$journeyEnd)){
				$idx = $jidx;
			}
			if ($when > $journeyEnd){ // we've finished this one
				$last = $jidx;
			}
			if ($when < $journeyStart){ // yet to start this one
				if ($next === -1) // we only want the first one yet to be started
					$next = $jidx;
			}
		}
		if ($which < 0 && $last >-1)
			$idx = $last +1 + $which;
		if ($which > 0 && $next >-1)
			$idx = $next -1 + $which;
		if (($idx >= 0) && $idx < ($this->journeys['totalCount']))
			return $this->journeys['items'][$idx];
		else
			return null; // out of bounds
	}

	/** @brief get a station's location
	  * @param array $station
	  * @return array{lat: float, lng: float}|null ['lat', 'lng'] on success, null otherwise
	  */
	public static function station_coords($station): ?array{
		if (!$station)
			return null;
		$lat = round($station['location']['coordinates']['latitudeDegrees']??0, 4);
		$lng = round($station['location']['coordinates']['longitudeDegrees']??0, 4);
		if ($lat && $lng)
			return ['lat'=>$lat, 'lng'=>$lng];
		return null;
	}

	/** @brief get a station on a journey based upon GPS if available, otherwise time
	  * @param array $journey the journey to look at
	  * @param int $which which station (0=current, 1=next), default current
	  * @param array{lat: float, lng: float}|null $where gps coords ['lat', 'lng'], default current position
	  * @param int $when time to base it on if gps not available, default current time
	  */
	public static function get_station($journey, $which=self::CURRENT, $where=null, $when=null): ?array{
		if (!$journey || !($journey['journeyLocations']['totalCount']??null))
			return log_message(LOG_DEBUG, "No journey: ".print_c($journey['journeyLocations']['totalCount'], true), null);
		if (!$when)
			$when = time();
		if (!$where)
			$where = Gps::get_gps_data();
		$leeway = 1*60; // 1 minutes.
		$last = -1;
		$next = -1;
		$idx = -1;

		if ($which == self::FIRST){
			return ['station'=>$journey['journeyLocations']['items'][0], 'gps'=>$where];
		}
		if ($which == self::LAST){
			$last = $journey['journeyLocations']['totalCount'] -1;
			return ['station'=>$journey['journeyLocations']['items'][$last], 'gps'=>$where];
		}

		foreach($journey['journeyLocations']['items'] as $sidx => $station){
			if ($station['type'] !== 'STOP')
				continue;
			if ( isset($where['lat']) &&  ($station_distance = Gps::distance(self::station_coords($station), $where)) !== null){
				// we have gps
				$proximityRadiusMetres = $station['location']['proximityRadiusMetres'];
				if ($station_distance < $proximityRadiusMetres){
					// we're at a station according to GPS
					$idx = $sidx;
					$last = $sidx-1;
					$next = $sidx+1;
					break; // exit loop
				}
			}
			$arrival = self::station_epoch($station, self::ARRIVAL) - $leeway;
			$departure = self::station_epoch($station, self::DEPARTURE) + $leeway;
			$code = $station['location']['code'];
			// log_message(LOG_DEBUG, "[$sidx]$code: ".utc($arrival).' - '.utc($departure));
			if (($when > $arrival) && ($when <$departure)){
				$idx = $sidx;
				// log_message(LOG_DEBUG, "idx=$idx");
			}
			if ($when > $departure){ // we've passed this one
				$last = $sidx;
				// log_message(LOG_DEBUG, "last=$last");
			}
			if ($when < $arrival){ // yet to arrive here
				if ($next === -1){ // we only want the first one
					$next = $sidx;
					// log_message(LOG_DEBUG, "next=$next");
				}
			}
		}

		if ($which < 0 && $last >-1)
			$idx = $last +1 + $which;
		if ($which > 0 && $next >-1)
			$idx = $next -1 + $which;
		if (($idx >= 0) && $idx < ($journey['journeyLocations']['totalCount'])){
			return ['station'=>$journey['journeyLocations']['items'][$idx], 'gps'=>$where];
		}else{
			return null; // out of bounds
		}
	}

	/** @brief debug-out a station object
	  * @param array $station the station to dump
	  * @param bool $return false(default) output to stdout, true - return as string
	  * @return string|bool
	  */
	public static function debug_station($station, $return=false){
		$type = $station['type'];
		$arrive = self::station_epoch($station, self::ARRIVAL);
		$depart = self::station_epoch($station, self::DEPARTURE);
		$code = $station['location']['code'];
		$lat = round($station['location']['coordinates']['latitudeDegrees']??0, 4);
		$lng = round($station['location']['coordinates']['longitudeDegrees']??0, 4);
		$msg = "[$type] $code: ".utc($arrive)."/".utc($depart)." $lat:$lng";
		if ($return)
			return $msg;
		return log_message(LOG_DEBUG, $msg, false);
	}

	/** @brief debug-out a journey object
	  * @param array $journey the station to dump
	  * @return void
	  */
	public static function debug_journey($journey){
		if (!$journey)
			log_message(LOG_DEBUG, "NULL");
		$journeyStart = epoch($journey['origin']['plannedDepartureDateTime']);
		$journeyEnd = epoch($journey['destination']['plannedArrivalDateTime']);
		$serviceCode = $journey['serviceCode'];
		$journeyCode = $journey['journeyCode'];
		log_message(LOG_DEBUG, "$serviceCode: ".utc($journeyStart)." - ".utc($journeyEnd));
	}

	/** @brief debug-out all journeys
	  * @return void
	  */
	public function debug_journeys(){
		if (!$this->journeys || !($this->journeys['items']??null))
			log_message(LOG_DEBUG, "No journeys", null);
		foreach($this->journeys['items'] as $jidx => $journey){
			self::debug_journey($journey);
		}
	}

	/** @brief debug-out a stations on a journey
	  * @param array $journey the journey to dump
	  * @return void
	  */
	public static function debug_stations($journey){
		if (!$journey)
			log_message(LOG_DEBUG, "No journey");
		foreach($journey['journeyLocations']['items'] as $sidx => $station){
			self::debug_station($station);
		}
	}

	/** @brief set journeys helper
	  * @param array $journeys
	  * @return void
	  */
	function setjourneys($journeys){
		$this->journeys = $journeys;
		$this->sort();
	}

	/** @brief ctor
	  * @param array $journeys
	  * @return void
	  */
	function __construct($journeys){
		$this->setjourneys($journeys);
	}
}

class Vehicle{
	/** @var string */ private $designation;
	/** @var array|null */ private $consist;
	/** @var Journeys|null */ private $journeys;

	/** @brief ctor
	  * @param string $designation - designation of the train
	  * @return void
	  */
	function __construct(string $designation){
		$this->designation($designation);
	}

	/** Get the internal consist ID for this consist
	  */
	public function consist_id(): ?string {
		if ($this->consist){
			if (($this->consist['totalCount']??0) > 0){
				return $this->consist['items'][0]['id']??null;
			}
		}
		return log_message(LOG_DEBUG, "No consist data", null);
	}

	/** Get the consist data, and if that works, get the journey assignments
	  */
	public function retrieve_data(): bool {
		static $timeout = 0; /* Stop this doing much if called too frequently */

		if (time() < $timeout){
			// too soon.
			return false;
		}

		$timeout = time() + 7*60 + rand(-120, 120); // allow again in 7m  ± 2m if we don't return journeys - we'll change this later on if we succeed.

		log_message(LOG_DEBUG, "Presuming no journeys, allow a rerun in ".($timeout-time())."s, ".utc($timeout));

		$this->consist = null;
		$this->journeys = null;

		if (!$this->designation)
			return log_message(LOG_ERR, "No designation set");

		if (!($this->consist = Web::consist($this->designation)))
			return log_message(LOG_ERR, "Couldn't get consist information");

		if (!($consist_id = $this->consist_id()))
			return log_message(LOG_ERR, "No consist data for ".$this->designation);

		if (!($journeys = Web::assignments($consist_id)))
			return log_message(LOG_ERR, "Couldn't get journey information for ". $this->consist_id());

		$this->journeys = new Journeys($journeys);

		$timeout = time() + 37*60 + rand(-5*60, 5*60); // 37m ± 5m if we do
		log_message(LOG_DEBUG, "Got journeys, allow a rerun in ".($timeout-time())."s, ".utc($timeout));

		return true;
	}

	/** @brief get/set the designation for this vehicle
	  * @param string $designation if provided, set the designation and get the train data
	  * @return string|null the current or old designation
	  */
	public function designation($designation = null): ?string{
		$tmp = $this->designation;
		if ($designation){
			log_message(LOG_DEBUG, "Setting designation to $designation");
			$this->designation = $designation;
			$this->retrieve_data();
		}
		return $tmp;
	}

	/** @brief get a journey based on time (wrapper for Journeys::get_journey())
	  * @param int $which which journey. 0 = now
	  * @param int $when omcpared to to when? Defaults to now
	  * @return array|null journey, if one fits the request, null otherwise ("NEXT" when on last, "CURRENT" when in between journeys etc.)
	  */
	public function get_journey($which = Journeys::CURRENT, $when = null): ?array{
		IF (!$this->journeys)
			return null;
		return $this->journeys->get_journey($which, $when);
	}
}

class Socket {
	/** @brief send a UDP packet
	  * @param array{ip:string, port:int , from:?int } $dest socket details
	  * @param string $data what to send
	  * @return bool true if sent, false otherwise
	  *
	  * If from is omitted, an ephemeral port is used
	  */
	public static function send_udp(array $dest, string $data){
		$ip = $dest['ip'];
		$port = intval($dest['port']);
		$from = intval($dest['from']??0);


		if ($socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)) {
			if ($from){
				if (!socket_bind($socket, "0.0.0.0", $from)){
					$errorcode = socket_last_error();
					$errormsg = socket_strerror($errorcode);
					return log_message(LOG_ERR, "Problem binding to 0.0.0.0:$from: $errormsg", false);
				}
			}
			$res = socket_sendto($socket, $data, strlen($data), 0, $ip, $port);
			// log_message(LOG_DEBUG, "sendto $from=>$ip:$port returned $res");
			socket_close($socket);
		}else{
			$errorcode = socket_last_error();
			$errormsg = socket_strerror($errorcode);
			return log_message(LOG_DEBUG, "Couldn't open socket: $errormsg", false);
		}
		return true;
	}
}


class SimpleDCP {
	/** @var Vehicle */ private $vehicle;

	/** @breif ctor
	  * @param Vehicle|null $vehicle
	  */
	function __construct($vehicle){
		$this->vehicle = $vehicle;
	}

	/** @brief pd Journey Information
	  * @param array{file:?string, udp:?array}|null $destinations where to send the XML, if provided
	  * @return void
	  */
	function dp(?array $destinations){
		if (!$destinations)
			return; // nothing to do
		/* Spec_DCP1.0_EN.0_EN 3.5.2
			If any data are not available, for example the GPS signal strength
			is too low or the GPS receiver broke, they must be omitted (i.e. no
			false or outdated information must be transmitted). */
		/*
		<dlx vers="1.0" type="pd">
			<wpt sat="12" lat="8.2651245" lon="-34.7963480"/>
			<speed val="70.0"/>
			<inp type="driving" val="1"/>
			<inp type="door_release_r" val="1"/>
			<inp type="door_release_l" val="0"/>
			<inp type="operational" val="1"/>
			<inp type="atstop" val="0"/>
			<pis stop="Berlin Hbf." line="ABC" trip="XYZ"/>
			<distance laststop="5375"/>
		</dlx>
		*/
		$xml = new SimpleXMLElement('<dlx/>');
		$xml->addAttribute('vers', '1.0');
		$xml->addAttribute('type', 'pd');

		$gps = Gps::get_gps_data();

		$wpt = $xml->addChild('wpt');
		$wpt->addAttribute('sat', strval($gps['sat_view']??0)); // sat_view|sat_use
		if (isset($gps['lat'])){
			/* Spec_DCP1.0_EN.0_EN  6.11.2 "If the positioning information is invalid, the lat and lon attributes must be omitted." */
			$wpt->addAttribute('lat', strval($gps['lat']??0));
			$wpt->addAttribute('lon', strval($gps['lng']??0));
		}

		$speed = $xml->addChild('speed');
		$speed->addAttribute('val', strval($gps['kph']??0));

		$inp = $xml->addChild('imp');
		$inp->addAttribute('type', 'driving');
		$inp->addAttribute('val', strval((($gps['kph']??0)>0)?1:0));

		if ( ($current_journey = $this->vehicle->get_journey())){
			$inp = $xml->addChild('inp');
			$inp->addAttribute('type', 'operational');
			$inp->addAttribute('val', strval(1));

			$atstop = (!!($station = $current = Journeys::get_station($current_journey)));
			$inp = $xml->addChild('inp');
			$inp->addAttribute('type', 'atstop');
			$inp->addAttribute('val', strval($atstop));

			if (!$atstop){ // get the next station
				$station = Journeys::get_station($current_journey, 1);
			}
			/* Next/current station */
			if ($station){
				$pis = $xml->addChild('pis');
				$pis->addAttribute('stop', $station['station']['location']['code']);
				$pis->addAttribute('trip', $current_journey['journeyCode']); // dunno about these two
				$pis->addAttribute('line', $current_journey['serviceCode']);
			}

			/* distance from previous station if known */
			if ( ($previous =Journeys::get_station($current_journey, -1)) && isset($gps['lat'])){
				$dist = Gps::distance(Journeys::station_coords($previous['station']), $gps);

				$distance = $xml->addChild('distance');
				$distance->addAttribute('laststop', strval($dist??0));
			}

		}else{
			$inp = $xml->addChild('inp');
			$inp->addAttribute('type', 'operational');
			$inp->addAttribute('val', strval(0));

			$inp = $xml->addChild('inp');
			$inp->addAttribute('type', 'atstop');
			$inp->addAttribute('val', strval(0));

			$pis = $xml->addChild('pis');
			$pis->addAttribute('stop', '');
			$pis->addAttribute('trip', '');
			$pis->addAttribute('line', '');

		}
		/* Send data */
		if ($path = $destinations['file']??null){
			if ($path[0]!=='/')
				$path = '/var/local/php_journey/'.$path;

			if (!$xml->asXML($path)){
				log_message(LOG_ERR, "Couldn't write to $path");
			}else{
				log_message(LOG_DEBUG, "Wrote $path");
			}
		}
		foreach($destinations['udp']??null as $udp){
			Socket::send_udp($udp, $xml->asXML());
		}

		$dom = dom_import_simplexml($xml)->ownerDocument;
		$dom->formatOutput = true;
		log_message(LOG_DEBUG, $dom->saveXML());

	}
}

/** START **/

$var_local = '/var/local/php_journey/';

if (!file_exists($var_local))
	mkdir($var_local, 0755, true);
umask(022);


if (!($designation = $_SERVER['JOURNEY_DESIGNATION']??exec('/usr/local/bin/glider_lookup ccu designation'))){
	log_message(LOG_ERR, "No designation set");
	echo "No designation set\n";
	exit(-1);
}
$vehicle = new Vehicle($designation);

$dcp = new SimpleDCP($vehicle);

if (file_exists('/conf/get_journey_data_config.php')){
	include('/conf/get_journey_data_config.php');
}

// $at_station = false;
$current_journey_code = '';
while (true){

	// Deal with DCP
	$dcp->dp($dcp_dp??null);

	// Update current journey data if required
	if ($current_journey = $vehicle->get_journey()){
		if ($current_journey_code != $current_journey['journeyCode']){
			// log_message(LOG_DEBUG, "current journey:".print_c($current_journey, true));
			$curJourney = [
				"trainID" => $current_journey['serviceCode'],
				"journey" => $current_journey['journeyLocations']['items'],
				"error" => 'No error',
				"journeyNumber" => $current_journey['jidx']
			];
			file_put_contents($var_local.'journey_stops.json', json_encode($curJourney));
			file_put_contents($var_local.'journey_stops.json.pp', json_encode($curJourney, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));

			$train_id = [
				"train_id" => $current_journey['serviceCode'],
				"error" => 'No error'
			];
			file_put_contents($var_local.'train_id.json', json_encode($train_id));
		}
		$current_journey_code = $current_journey['journeyCode'];
	}else{
		if ($current_journey_code != "none"){
			$curJourney = [
				"error" => 'No current journeys'
			];
			file_put_contents($var_local.'journey_stops.json', json_encode($curJourney));
			file_put_contents($var_local.'journey_stops.json.pp', json_encode($curJourney, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));

			$train_id = [
				"train_id" => '',
				"error" => 'No error'
			];
			file_put_contents($var_local.'train_id.json', json_encode($train_id));
		}
		$current_journey_code = "none";
	}

	if (!($gps = Gps::get_gps_data())){
		$gps = [
			"error" => 'No GPS'
		];
	}
	file_put_contents($var_local.'gps.json', json_encode($gps));
	file_put_contents($var_local.'gps.json.pp', json_encode($gps, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));

	$vehicle->retrieve_data(); // takes care of it's own caching/not running too much
	sleep(5);

	// if ($current_journey = $vehicle->get_journey()){
	// 	log_message(LOG_DEBUG, "Currently on ".$current_journey['journeyCode']);
	// 	$leave = $arrive = $end = 0;

	// 	/* Where was the start? */
	// 	if ($first = Journeys::get_station($current_journey, Journeys::FIRST)){
	// 		log_message(LOG_DEBUG, "First ".Journeys::debug_station($first['station'], true)." ".Gps::debug_gps($first['gps'], true));
	// 	}


	// 	/* Were are we now? */
	// 	if ($current = Journeys::get_station($current_journey)){
	// 		$at_station = true;
	// 		$leave = Journeys::station_epoch($current['station'], Journeys::DEPARTURE) - time();
	// 		if ($leave > 0){
	// 			$msg = " Leave in $leave s";
	// 		}else{
	// 			$msg = " Left ".($leave*-1)." s ago";
	// 		}
	// 		log_message(LOG_DEBUG, "Currently at ".Journeys::debug_station($current['station'], true)." ".Gps::debug_gps($current['gps'], true).$msg);

	// 	}

	// 	/* Where are we off to next? */
	// 	if ($next = Journeys::get_station($current_journey, 1)){
	// 		$arrive = Journeys::station_epoch($next['station'], Journeys::ARRIVAL) - time();
	// 		log_message(LOG_DEBUG, "Next ".Journeys::debug_station($next['station'], true)." ".Gps::debug_gps($next['gps'], true)." arrive in $arrive s");
	// 	}

	// 	/* And where are we going to end up */
	// 	if ($last = Journeys::get_station($current_journey, Journeys::LAST)){
	// 		$end = Journeys::station_epoch($last['station'], Journeys::ARRIVAL) - time();
	// 		log_message(LOG_DEBUG, "Last ".Journeys::debug_station($last['station'], true)." ".Gps::debug_gps($last['gps'], true));
	// 	}

	// 	/* Sleep, depending on where we are and what's next */
	// 	if ($leave){ // at a station, sleep until we're just about to leave. Or a minute. */
	// 		sleep(clamp($leave-5, 5, 60));;
	// 	}else if ($arrive){
	// 		if ($at_station){ /* we've just left a station */
	// 			sleep(10); // at least one more cycle to clear "current station" if required
	// 			$at_station = false;
	// 		}else{
	// 			sleep(clamp($arrive-5, 10, 60)); /* Sleep until we're just about to arrive */
	// 		}
	// 	}else{
	// 		sleep(10);
	// 	}

	// }else{
	// 	if ($next_journey = $vehicle->get_journey(Journeys::NEXT)){
	// 		$start = epoch($next_journey['origin']['plannedDepartureDateTime']??"2037-12-31T23:59:59Z");
	// 		log_message(LOG_DEBUG, "No current journey, next one starts at ".utc($start)." GPS: ".Gps::debug_gps(Gps::get_gps_data(), true));
	// 		sleep(10); // TODO: change to sleep just before it.
	// 	}else{
	// 		log_message(LOG_DEBUG, "No more journeys. Wait until tomorrow?");
	// 		sleep(10); // TOOD: Sleep until midnight/an hour?
	// 		$vehicle->retrieve_data();
	// 	}
	// }

}
