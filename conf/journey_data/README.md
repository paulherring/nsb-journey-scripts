## General

Debug can be enabled by removing/renaming `/conf/.nd2syslog_disable_hack`. Doing so will take effect immediately for this, it doesn't need to be restarted.

## Classes in get_journey_data.php

### Global

- `set_error_handler()` to catch errors
- `log_message()` - syslog() similucrom to log source lines with messages to error_log()
- `print_c()` - more compact version of `print_r()` (Uses `internal_compact_dump()`)
- `utc()` - returns a RFC3339 UTC time, from a variety of input formats (string, DateTime, epoc seconds)
- `epoch()` - returns epoch seconds, from a RFC3339 time format string

### Gps
- Obtains data from various /var/local/gps.default/gp* files
- Provides a haversine distance function, between either (single point, here) or (first point, second point)

### Web
- Provides cached web queries for the two URL's used

### Journeys
- Manages a set of journeys
- `station_epoch()` - obtain station time (arrive/depart/pass depending on station type/what's requested)
- `get_journey()` - get a particular journey (current, previous, next, first, last)
- `get_station()`- get a particular station within a journey (current, previous, next, first, last)

### Vehicle
- Represents a train
- Requires designation
- Gets both train and journey data

### Socket
- Only handles UDP sending at the moment

### SimpleDCP
- Only handles the pd (Process Data) XML type at the moment


